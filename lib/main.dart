import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import 'app/app.dart';

Future<void> _initAppInitials() async {
  WidgetsFlutterBinding.ensureInitialized();
  HttpOverrides.global = MyHttpOverrides();
}

Future<void> main() async {
  await _initAppInitials();

  runApp(const AppRoot());
}

class AppRoot extends StatelessWidget {
  const AppRoot({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: AviBusApp(),
    );
  }
}

class AviBusApp extends StatefulWidget {
  const AviBusApp({super.key});

  @override
  State<AviBusApp> createState() => _AviBusAppState();
}

class _AviBusAppState extends State<AviBusApp> {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);

    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) {
        return DismissKeyboard(
          child: ScreenUtilInit(
            useInheritedMediaQuery: true,
            designSize: Size(constraints.maxWidth, constraints.maxHeight),
            builder: (_, child) => GetMaterialApp(
              initialRoute: AppRoutes.login,
              getPages: AppPages.list,
              debugShowCheckedModeBanner: false,
              theme: AppTheme.light,
              darkTheme: AppTheme.dark,
              themeMode: ThemeMode.light,
            ),
          ),
        );
      },
    );
  }
}
