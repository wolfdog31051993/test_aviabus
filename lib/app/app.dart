library app;

export 'screen/screen.dart';
export 'data/data.dart';
export 'core/core.dart';
export 'global/global.dart';
