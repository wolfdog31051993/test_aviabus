import 'package:avibus/app/app.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

// ignore: must_be_immutable
class GlobalSearchCard extends StatefulWidget {
  final Function(String routeA, String routeB, String date) onClick;
  bool isCheck;
  GlobalSearchCard({super.key, required this.onClick, this.isCheck = false});

  @override
  State<GlobalSearchCard> createState() => _GlobalSearchCardState();
}

class _GlobalSearchCardState extends State<GlobalSearchCard> {
  final TextEditingController routeAController = TextEditingController();

  final TextEditingController routeBController = TextEditingController();

  final TextEditingController dateController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          color: Colors.white,
          padding: const EdgeInsets.all(15.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  Container(
                    margin: EdgeInsets.only(right: 10.w),
                    child: Column(
                      children: [
                        const Icon(
                          Icons.location_on_outlined,
                          color: ThemeColor.primaryColor,
                        ),
                        RotatedBox(
                          quarterTurns: 1,
                          child: SizedBox(
                            width: 0.2.sw,
                            child: const MySeparator(),
                          ),
                        ),
                        const Icon(
                          Icons.location_on,
                          color: ThemeColor.primaryColor,
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                      child: Column(
                    children: [
                      MyTextFormField(
                        controller: routeAController,
                        label: 'Откуда',
                        hintText: 'Откуда',
                        inputType: TextInputType.text,
                        suffix: const Icon(
                          Icons.gps_fixed,
                          color: ThemeColor.primaryColor,
                        ),
                      ),
                      SizedBox(height: 15.h),
                      MyTextFormField(
                        controller: routeBController,
                        label: 'Куда',
                        hintText: 'Куда',
                        inputType: TextInputType.text,
                        suffix: const Icon(
                          Icons.gps_fixed,
                          color: ThemeColor.primaryColor,
                        ),
                      )
                    ],
                  ))
                ],
              ),
              SizedBox(height: 15.h),
              MyTextFormField(
                controller: dateController,
                label: 'Дата',
                hintText: 'Дата',
                inputType: TextInputType.text,
                readOnly: true,
                suffix: const Icon(
                  Icons.calendar_month,
                  color: ThemeColor.primaryColor,
                ),
                onTap: () => showDate(context),
              ),
              Row(
                children: [
                  Checkbox(
                    value: widget.isCheck,
                    shape: const CircleBorder(),
                    onChanged: (val) {
                      setState(() {
                        widget.isCheck = val!;
                      });
                    },
                  ),
                  Text(
                    'Передать посылку',
                    style: TextStyle(
                      fontWeight: FontWeight.w400,
                      fontSize: 15.sp,
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
        Container(
          padding: const EdgeInsets.all(16),
          child: PrimaryButton(
              onPressed: () => widget.onClick(routeAController.text,
                  routeBController.text, dateController.text),
              text: 'Найти'),
        )
      ],
    );
  }

  Future<void> showDate(context) async {
    final DateTime dtNow = DateTime.now();
    DateTime? selectedDate = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: dtNow,
        //DateTime.now() - not to allow to choose before today.
        lastDate: DateTime(2100));

    if (selectedDate != null) {
      setState(() {
        dateController.text = dateFormatter(selectedDate.toString());
      });
    }
  }
}
