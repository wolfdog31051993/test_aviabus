import 'package:get/get.dart';

import '../../app.dart';

class HomeController extends GetxController {
  final state = HomeState();

  void onChangePage(int index) {
    state.indexPage.value = index;
  }

  Future<void> onSearch(String routeA, String routeB, String date) async {
    state.isLoading(true);

    Map<String, dynamic> params = {
      'departure_city': routeA,
      'destination_city': routeB,
      'date': date
    };

    final result = await GetRouteApi.get(params);

    state.isLoading(false);

    if (result == null) {
      return;
    } else {
      state.indexPage.value == 0
          ? state.passengerTripsList.value = result
          : state.driverTripsList.value = result;
    }
  }
}
