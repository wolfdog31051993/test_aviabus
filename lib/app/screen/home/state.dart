import 'package:get/get.dart';

import '../../data/model/trips.dart';

class HomeState {
  RxBool isLoading = false.obs;
  RxBool isHasError = false.obs;

  RxInt indexPage = 0.obs;

  RxBool isCheckCargo = false.obs;

  RxList<Trips> passengerTripsList = <Trips>[].obs;
  RxList<Trips> driverTripsList = <Trips>[].obs;
}
