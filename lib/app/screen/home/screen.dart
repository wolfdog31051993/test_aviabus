import 'package:avibus/app/app.dart';
import 'package:avibus/app/screen/home/widget/driver.dart';
import 'package:avibus/app/screen/home/widget/switch_btn.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import 'widget/passenger.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return GetX<HomeController>(
      init: HomeController(),
      builder: (hc) {
        return Scaffold(
          backgroundColor: ThemeColor.homeBackColor,
          body: SafeArea(
              child: Column(
            children: [
              Container(
                color: Colors.white,
                child: Padding(
                  padding: const EdgeInsets.all(18.0),
                  child: Column(
                    children: [
                      SizedBox(
                        width: double.infinity,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              'Поиск поездок',
                              style: TextStyle(
                                  fontWeight: FontWeight.w500, fontSize: 18.sp),
                            ),
                            const Icon(Icons.draw)
                          ],
                        ),
                      ),
                      SizedBox(height: 15.0.h),
                      const HomeSwitchBtn(),
                    ],
                  ),
                ),
              ),
              if (hc.state.indexPage.value == 0)
                const Expanded(child: PassengerScreen()),
              if (hc.state.indexPage.value == 1)
                const Expanded(child: DriverScreen())
            ],
          )),
        );
      },
    );
  }
}
