import 'package:avibus/app/app.dart';
import 'package:avibus/app/screen/home/widget/trips_item.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class DriverScreen extends StatelessWidget {
  const DriverScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return GetX<HomeController>(
        init: HomeController(),
        builder: (hc) {
          return IgnorePointer(
            ignoring: hc.state.isLoading.value,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  GlobalSearchCard(
                    onClick: (String routeA, String routeB, String date) =>
                        hc.onSearch(routeA, routeB, date),
                    isCheck: hc.state.isCheckCargo.value,
                  ),
                  Container(
                    color: ThemeColor.homeBackColor,
                    padding: const EdgeInsets.all(16),
                    child: hc.state.isLoading.value
                        ? const CircularProgressIndicator()
                        : ListView.builder(
                            shrinkWrap: true,
                            physics: const BouncingScrollPhysics(),
                            itemCount: hc.state.driverTripsList.length,
                            itemBuilder: (context, index) {
                              return TripsItem(
                                  trips: hc.state.driverTripsList[index]);
                            },
                          ),
                  )
                ],
              ),
            ),
          );
        });
  }
}
