import 'package:avibus/app/core/core.dart';
import 'package:avibus/app/screen/home/home.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

class HomeSwitchBtn extends StatelessWidget {
  const HomeSwitchBtn({super.key});

  @override
  Widget build(BuildContext context) {
    return GetX<HomeController>(
      init: HomeController(),
      builder: (hc) {
        return Container(
          height: 45.h,
          width: double.infinity,
          decoration: const BoxDecoration(
            borderRadius: BorderRadius.all(
              Radius.circular(16.0),
            ),
          ),
          child: Row(
            children: [
              Expanded(
                child: InkWell(
                  onTap: () => hc.onChangePage(0),
                  child: Container(
                    alignment: Alignment.center,
                    width: double.infinity,
                    height: 45.h,
                    decoration: BoxDecoration(
                        borderRadius: const BorderRadius.only(
                            topLeft: Radius.circular(16.0),
                            bottomLeft: Radius.circular(16.0)),
                        color: hc.state.indexPage.value == 0
                            ? ThemeColor.primaryColor
                            : const Color.fromARGB(255, 230, 230, 230)),
                    child: Text(
                      'Пассажир',
                      style: TextStyle(
                        color: hc.state.indexPage.value == 0
                            ? ThemeColor.whiteColor
                            : ThemeColor.blackColor,
                        fontSize: 15.sp,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
              ),
              Expanded(
                child: InkWell(
                  onTap: () => hc.onChangePage(1),
                  child: Container(
                    alignment: Alignment.center,
                    width: double.infinity,
                    height: 45.h,
                    decoration: BoxDecoration(
                        borderRadius: const BorderRadius.only(
                            topRight: Radius.circular(16.0),
                            bottomRight: Radius.circular(16.0)),
                        color: hc.state.indexPage.value == 1
                            ? ThemeColor.primaryColor
                            : const Color.fromARGB(255, 230, 230, 230)),
                    child: Text(
                      'Водитель',
                      style: TextStyle(
                        color: hc.state.indexPage.value == 1
                            ? ThemeColor.whiteColor
                            : ThemeColor.blackColor,
                        fontSize: 15.sp,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
              )
            ],
          ),
        );
      },
    );
  }
}
