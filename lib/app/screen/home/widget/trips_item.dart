import 'package:avibus/app/data/model/trips.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../app.dart';

class TripsItem extends StatelessWidget {
  final Trips trips;
  const TripsItem({super.key, required this.trips});

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Padding(
        padding: const EdgeInsets.all(10),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      Container(
                        margin: const EdgeInsets.only(right: 5),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(100),
                          color: Colors.white,
                          border: Border.all(
                            color: Colors.green,
                            width: 2,
                          ),
                        ),
                        width: 70,
                        height: 70,
                        padding: const EdgeInsets.all(2),
                        child: Container(
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            image: DecorationImage(
                              image: Image.network(
                                      "https://images.unsplash.com/photo-1612151855475-877969f4a6cc?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8aGQlMjBpbWFnZXxlbnwwfHwwfHw%3D&w=400&q=80")
                                  .image,
                              fit: BoxFit.fitHeight,
                            ),
                          ),
                        ),
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            '${trips.carrier}',
                            style: TextStyle(
                              fontWeight: FontWeight.w600,
                              fontSize: 14.sp,
                            ),
                          ),
                          Row(
                            children: [
                              SizedBox(
                                width: 0.26.sw,
                                child: Text(
                                  '${trips.bus!.name}',
                                  style: TextStyle(
                                      fontWeight: FontWeight.w400,
                                      fontSize: 14.sp,
                                      overflow: TextOverflow.ellipsis),
                                  maxLines: 2,
                                ),
                              ),
                              Text(
                                ' - ${trips.passengerFareCost} ${trips.currency}',
                                style: TextStyle(
                                  fontWeight: FontWeight.w600,
                                  fontSize: 14.sp,
                                ),
                              ),
                            ],
                          ),
                        ],
                      )
                    ],
                  ),
                  SizedBox(height: 10.h),
                  Row(
                    children: [
                      Container(
                        margin: const EdgeInsets.only(right: 10),
                        child: Column(
                          children: [
                            const Icon(
                              Icons.location_on_outlined,
                              color: ThemeColor.primaryColor,
                            ),
                            RotatedBox(
                              quarterTurns: 1,
                              child: SizedBox(
                                width: 0.2.sw,
                                child: const MySeparator(),
                              ),
                            ),
                            const Icon(
                              Icons.location_on,
                              color: ThemeColor.primaryColor,
                            ),
                          ],
                        ),
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            '${trips.departure!.name}',
                            style: TextStyle(
                              fontWeight: FontWeight.w600,
                              fontSize: 14.sp,
                            ),
                          ),
                          SizedBox(
                            width: 0.26.sw,
                            child: Text(
                              dateTimeFormatter('${trips.departureTime}'),
                              style: TextStyle(
                                fontWeight: FontWeight.w400,
                                fontSize: 14.sp,
                              ),
                              maxLines: 2,
                            ),
                          ),
                          const SizedBox(height: 15),
                          Text(
                            '${trips.destination!.name}',
                            style: TextStyle(
                              fontWeight: FontWeight.w600,
                              fontSize: 14.sp,
                            ),
                          ),
                          SizedBox(
                            width: 0.26.sw,
                            child: Text(
                              dateTimeFormatter('${trips.arrivalTime}'),
                              style: TextStyle(
                                fontWeight: FontWeight.w400,
                                fontSize: 14.sp,
                              ),
                              maxLines: 2,
                            ),
                          ),
                        ],
                      )
                    ],
                  )
                ],
              ),
            ),
            const Icon(
              Icons.bookmark_add_outlined,
              color: ThemeColor.primaryColor,
            )
          ],
        ),
      ),
    );
  }
}
