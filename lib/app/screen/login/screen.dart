import 'package:avibus/app/app.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

class LoginScreen extends StatelessWidget {
  const LoginScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return GetBuilder<LoginController>(
        init: LoginController(),
        builder: (lc) {
          return Scaffold(
            body: Container(
              margin:
                  const EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
              height: double.infinity,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  SizedBox(height: 15.h),
                  PrimaryButton(
                      onPressed: () => Get.to(() => const HomeScreen()),
                      text: 'Get Started'),
                  SizedBox(height: 15.h),
                  PrimaryButton(
                    onPressed: () {},
                    text: 'Terms and Conditions, Privacy Policy',
                    color: Colors.grey,
                  ),
                ],
              ),
            ),
          );
        });
  }
}
