// converts date time string to format dd.MM.yyyy

import 'package:intl/intl.dart';

String dateTimeFormatter(String dateTime) {
  return DateFormat('dd.MM.yyyy, HH:mm')
      .format(DateTime.parse(dateTime).add(const Duration(hours: 5)));
}

String dateTimeFormatterCourier(String dateTime) {
  return DateFormat('yyyy-MM-dd HH:mm').format(DateTime.parse(dateTime));
}

// converts date time string to format HH:mm
String timeFormatterForTicket(String dateTime) {
  return DateFormat('HH:mm').format(
    DateTime.parse(dateTime).add(const Duration(hours: 5)),
  );
}

// converts date to dd.MM.yyyy format
String dateFormatter(String dateTime) {
  return DateFormat('yyyy-MM-dd').format(DateTime.parse(dateTime));
}

// String dateForSearchTicket(String dateTime) {
//   return DateFormat('yyyy-MM-dd').format(DateTime.parse(dateTime));
// }

/// converts dd.MM.yyyy to yyyy-MM-dd [String format]
String convertDotDateToDateStr(String dateStr) {
  final List<String> dates = dateStr.split('.');
  final String day = dates[0];
  final String month = dates[1];
  final String year = dates[2];

  return '$year-$month-$day';
}

/// converts dd-MM-yyyy format to yyyy-MM-dd [Date format]
DateTime convertStrToDate(String dateStr) {
  final List<String> dates = dateStr.split('.');
  final String year = dates[2];
  final String month = dates[1];
  final String day = dates[0];

  dateStr = '$year-$month-$day';

  return DateTime.parse(dateStr);
}
