import 'package:flutter/material.dart';

class ThemeColor {
  //Static Colors
  static const whiteColor = Colors.white;
  static const blackColor = Colors.black;
  static const greenColor = Color(0xFF029502);
  static const primaryColor = Color.fromARGB(255, 130, 165, 127);
  static const dividerColor = Color(0xFF5F5F5F);

  static Color ticketInfoBackground =
      const Color.fromARGB(255, 60, 165, 246).withOpacity(0.1);

  // LightColors
  static const lightBgColor = Color.fromARGB(52, 93, 169, 255);
  static const lightTextColor = Color(0xFF333333);
  static const lightTicketsIconColor = Color(0xFF333333);
  static const lightUncheckedCalendarColor = Color(0xFFDCDCDC);
  static const lightMainContainerColor = Colors.white;
  static const lightBottomNavColor = Colors.white;
  static const lightBodyTextColor = Color(0xFFB8B8B8);
  // Dark Colors
  static const darkTextColor = Colors.white;
  static const darkMainColor = Color.fromARGB(255, 26, 28, 37);
  static const darkTicketsIconColor = Colors.white;
  static const darkUncheckedCalendarColor = Color(0xFF707070);
  static const darkMainContainerColor = Color.fromARGB(255, 22, 27, 36);
  static const darkBottomNavColor = Color(0xFF222222);
  // Widgets Colors
  static const errorBtnColor = Color.fromARGB(255, 229, 89, 89);
  static const bottomSelectedColor = Color(0xFF7B72F1);
  static const bottomUnSelectedColor = Color(0xFF929292);
  static const infoTextColor = Color(0xFF5F5F5F);
  static const infoButtonColor = Color(0xFFF5D182);
  static const confColor = Color(0xFF09B22E);
  static const uncheckedGenderColor = Color(0xFF222222);
  static const profileIconsColor = Color(0xFF85BCF1);
  static const cancelBtnColor = Color.fromARGB(255, 229, 89, 89);
  static const addCount = Color(0xFF212135);

  // Tickets background color light
  static const ticketContainerLight = Color.fromARGB(255, 250, 248, 242);

  // Gradient Colors
  static const gradient = LinearGradient(
    colors: [
      // Color(0xFFD1BDFF),
      // Color(0xFFA9C3C7),
      Color.fromARGB(255, 117, 184, 255),
      Color.fromARGB(255, 55, 140, 251),
      // Color(0xFF8c7ef0),
      // Color(0xFF9381ef),
      // Color(0xFF9c87ee),
      // Color(0xFFb296eb),
      // Color(0xFFB99AEA),
    ],
  );

  //Theatre seats colors
  static const haveSeatColor = Color(0xFF2D5C37);
  static const noSeatColor = Color(0xFFE05050);
  static const reservedSeatColor = Color(0xFF7B72F1);

  // error color
  static const errorColor = Color.fromARGB(255, 229, 89, 89);

  static const homeBackColor = Color.fromARGB(255, 236, 236, 236);
}
