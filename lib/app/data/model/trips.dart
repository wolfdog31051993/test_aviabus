import 'model.dart';

class TipsModel {
  List<Trips>? trips;

  TipsModel({this.trips});

  TipsModel.fromJson(Map<String, dynamic> json) {
    if (json['trips'] != null) {
      trips = <Trips>[];
      json['trips'].forEach((v) {
        trips!.add(Trips.fromJson(v));
      });
    }
  }
}

class Trips {
  String? id;
  String? routeId;
  String? scheduleTripId;
  String? routeName;
  String? routeNum;
  String? carrier;
  Bus? bus;
  String? driver1;
  String? frequency;
  String? status;
  String? statusPrint;
  Departure? departure;
  String? departureTime;
  String? arrivalToDepartureTime;
  Departure? destination;
  String? arrivalTime;
  String? distance;
  int? duration;
  bool? transitSeats;
  int? freeSeatsAmount;
  String? passengerFareCost;
  int? platform;
  bool? onSale;
  bool? additional;
  bool? aCBPDP;
  String? currency;
  String? principalTaxId;
  CarrierData? carrierData;
  String? passengerFareCostAvibus;

  Trips(
      {this.id,
      this.routeId,
      this.scheduleTripId,
      this.routeName,
      this.routeNum,
      this.carrier,
      this.bus,
      this.driver1,
      this.frequency,
      this.status,
      this.statusPrint,
      this.departure,
      this.departureTime,
      this.arrivalToDepartureTime,
      this.destination,
      this.arrivalTime,
      this.distance,
      this.duration,
      this.transitSeats,
      this.freeSeatsAmount,
      this.passengerFareCost,
      this.platform,
      this.onSale,
      this.additional,
      this.aCBPDP,
      this.currency,
      this.principalTaxId,
      this.carrierData,
      this.passengerFareCostAvibus});

  Trips.fromJson(Map<String, dynamic> json) {
    id = json['Id'];
    routeId = json['RouteId'];
    scheduleTripId = json['ScheduleTripId'];
    routeName = json['RouteName'];
    routeNum = json['RouteNum'];
    carrier = json['Carrier'];
    bus = json['Bus'] != null ? Bus.fromJson(json['Bus']) : null;
    driver1 = json['Driver1'];
    frequency = json['Frequency'];
    status = json['Status'];
    statusPrint = json['StatusPrint'];
    departure = json['Departure'] != null
        ? Departure.fromJson(json['Departure'])
        : null;
    departureTime = json['DepartureTime'];
    arrivalToDepartureTime = json['ArrivalToDepartureTime'];
    destination = json['Destination'] != null
        ? Departure.fromJson(json['Destination'])
        : null;
    arrivalTime = json['ArrivalTime'];
    distance = json['Distance'];
    duration = json['Duration'];
    transitSeats = json['TransitSeats'];
    freeSeatsAmount = json['FreeSeatsAmount'];
    passengerFareCost = json['PassengerFareCost'];

    platform = json['Platform'];
    onSale = json['OnSale'];

    additional = json['Additional'];

    aCBPDP = json['ACBPDP'];
    currency = json['Currency'];
    principalTaxId = json['PrincipalTaxId'];
    carrierData = json['CarrierData'] != null
        ? CarrierData.fromJson(json['CarrierData'])
        : null;
    passengerFareCostAvibus = json['PassengerFareCostAvibus'];
  }
}
