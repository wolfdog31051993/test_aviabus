import 'model.dart';

class CarrierData {
  String? carrierName;
  String? carrierTaxId;
  String? carrierStateRegNum;
  List<CarrierPersonalData>? carrierPersonalData;
  String? carrierAddress;
  String? carrierWorkingHours;

  CarrierData(
      {this.carrierName,
      this.carrierTaxId,
      this.carrierStateRegNum,
      this.carrierPersonalData,
      this.carrierAddress,
      this.carrierWorkingHours});

  CarrierData.fromJson(Map<String, dynamic> json) {
    carrierName = json['CarrierName'];
    carrierTaxId = json['CarrierTaxId'];
    carrierStateRegNum = json['CarrierStateRegNum'];
    if (json['CarrierPersonalData'] != null) {
      carrierPersonalData = <CarrierPersonalData>[];
      json['CarrierPersonalData'].forEach((v) {
        carrierPersonalData!.add(CarrierPersonalData.fromJson(v));
      });
    }
    carrierAddress = json['CarrierAddress'];
    carrierWorkingHours = json['CarrierWorkingHours'];
  }
}
