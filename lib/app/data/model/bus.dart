class Bus {
  String? id;
  String? model;
  String? licensePlate;
  String? name;
  String? seatsClass;
  int? seatCapacity;
  int? standCapacity;
  int? baggageCapacity;

  Bus({
    this.id,
    this.model,
    this.licensePlate,
    this.name,
    this.seatsClass,
    this.seatCapacity,
    this.standCapacity,
    this.baggageCapacity,
  });

  Bus.fromJson(Map<String, dynamic> json) {
    id = json['Id'];
    model = json['Model'];
    licensePlate = json['LicencePlate'];
    name = json['Name'];
    seatsClass = json['SeatsClass'];
    seatCapacity = json['SeatCapacity'];
    standCapacity = json['StandCapacity'];
    baggageCapacity = json['BaggageCapacity'];
  }
}
