class ValueVariants {
  String? name;

  ValueVariants({
    this.name,
  });

  ValueVariants.fromJson(Map<String, dynamic> json) {
    name = json['Name'];
  }
}
