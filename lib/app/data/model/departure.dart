class Departure {
  String? name;
  String? code;
  String? id;
  String? country;
  String? region;
  bool? automated;
  bool? hasDestinations;
  String? uTC;
  String? gPSCoordinates;
  String? locationType;
  String? address;

  Departure({
    this.name,
    this.code,
    this.id,
    this.country,
    this.region,
    this.automated,
    this.hasDestinations,
    this.uTC,
    this.gPSCoordinates,
    this.locationType,
    this.address,
  });

  Departure.fromJson(Map<String, dynamic> json) {
    name = json['Name'];
    code = json['Code'];
    id = json['Id'];
    country = json['Country'];
    region = json['Region'];
    automated = json['Automated'];
    hasDestinations = json['HasDestinations'];
    uTC = json['UTC'];
    gPSCoordinates = json['GPSCoordinates'];
    locationType = json['LocationType'];
    address = json['Address'];
  }
}
