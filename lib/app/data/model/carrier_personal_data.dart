import 'model.dart';

class CarrierPersonalData {
  String? name;
  String? caption;
  bool? mandatory;
  bool? personIdentifier;
  String? type;
  List<ValueVariants>? valueVariants;
  String? value;
  ValueVariants? defaultValueVariant;

  CarrierPersonalData({
    this.name,
    this.caption,
    this.mandatory,
    this.personIdentifier,
    this.type,
    this.valueVariants,
    this.value,
    this.defaultValueVariant,
  });

  CarrierPersonalData.fromJson(Map<String, dynamic> json) {
    name = json['Name'];
    caption = json['Caption'];
    mandatory = json['Mandatory'];
    personIdentifier = json['PersonIdentifier'];
    type = json['Type'];
    if (json['ValueVariants'] != null) {
      valueVariants = <ValueVariants>[];
      json['ValueVariants'].forEach((v) {
        valueVariants!.add(ValueVariants.fromJson(v));
      });
    }
    value = json['Value'];
    defaultValueVariant = json['DefaultValueVariant'] != null
        ? ValueVariants.fromJson(json['DefaultValueVariant'])
        : null;
  }
}
