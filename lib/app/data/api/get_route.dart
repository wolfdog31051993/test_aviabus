import 'package:avibus/app/core/util/util.dart';
import 'package:avibus/app/data/model/trips.dart';

class GetRouteApi {
  static Future<List<Trips>?> get(Map<String, dynamic> params) async {
    try {
      String path = '${Constants.baseUrl}/api/avibus/search_trips_cities/';
      final response =
          await HttpUtil().get(path: path, queryParameters: params);
      TipsModel model = TipsModel.fromJson(response);
      if (model.trips!.isNotEmpty) {
        return model.trips;
      } else {
        return [];
      }
    } catch (e) {
      return null;
    }
  }
}
